<?php

namespace App\Http\Controllers;

use App\Categorias;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * este metodo retorna la lista de categorias
     *
     */
    public function index()
    {
        $categorias = Categorias::all();

        return view('categorias.index')->with(compact('categorias'));
    }

    /**
     * Muestra el formulario para crear una nueva categoria
     *
     */
    public function create()
    {
        //
        return view('categorias.create');
    }

    /**
     * metodo se encarga de guardar en db los datos que vienen del formulario create
     *
     */
    public function store(Request $request)
    {
        //
        $categorias = new Categorias();
        $categorias->nombre_categoria = $request->input('nombre_categoria');
        $categorias->fecha_cracion = $request->input('fecha_creacion');
        $categorias->save();

        return redirect('categorias');
    }

    /**
     * Display the specified resource.
     *
     */
    public function show(Categorias $categorias)
    {
        //
    }

    /**
     * Muestra el formulario para un registro espesifico
     *
     */
    public function edit($id)
    {
        //
        $categoria = Categorias::find($id);

        return view('categorias.edit')->with(compact('categoria'));
    }

    /**
     * metodo modificia los valores entregados de la vista edit de un registro
     *
     */
    public function update(Request $request, $id)
    {
        //
        $categoria = Categorias::find($id);
        $categoria->nombre_categoria = $request->input('nombre_categoria');
        $categoria->fecha_cracion = $request->input('fecha_creacion');
        $categoria->save();

        return redirect('categorias');
    }

    /**
     * metodo elimina un registro espesifico
     *
     */
    public function destroy($id)
    {
        //
        $categoria = Categorias::find($id);
        $categoria->delete();

        return redirect('categorias');
    }
}
