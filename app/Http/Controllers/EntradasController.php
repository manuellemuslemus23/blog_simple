<?php

namespace App\Http\Controllers;

use App\User;
use App\Entradas;
use Carbon\Carbon;
use App\Categorias;
use App\Comentarios;
use Illuminate\Http\Request;

class EntradasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * este metodo retorna la lista de categorias
     *
     */
    public function index()
    {
         /*-------------------------------------------------------------------------
          *                Ordenando los registros de la db
          * -------------------------------------------------------------------------
          * El "reverse" método invierte el orden de los elementos de la colección
          * traida de la base de datos
          *
          *------------------------------------------------------------------------*/

        $entradas_orden_default = Entradas::all();
        $entradas = $entradas_orden_default->reverse();
        $entradas->all();

        $categorias = Categorias::all();

        return view('entradas.index')->with(compact('entradas','categorias'));
    }

    /**
     * Muestra el formulario para crear una nueva entrada
     *
     */
    public function create()
    {
        //
        $categorias = Categorias::all();

        return view('entradas.create')->with(compact('categorias'));
    }

    /**
     * guarda los datos en db obtenidos de la vista create de entradas
     *
     */
    public function store(Request $request)
    {
        //
        $entrada = new Entradas();
        $entrada->titulo = $request->input('titulo');
        $entrada->contenido = $request->input('contenido');
        if ($request->input('estado') == 2)
        {
            $fecha_actual = Carbon::now()->format('y-m-d');
            $entrada->fecha_publicacion = $fecha_actual;
        }
        $entrada->estado = $request->input('estado');
        $entrada->categorias_id = $request->input('id_categoria');
        $entrada->save();

        return redirect('/entradas/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entradas  $entradas
     * @return \Illuminate\Http\Response
     */
    public function show(Entradas $entradas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entradas  $entradas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $entrada = Entradas::find($id);
        $categorias = Categorias::all();
        $categoriaActual = Categorias::find($entrada->categorias_id);

        return view('entradas.edit')->with(compact('entrada','categorias','categoriaActual'));

    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, $id)
    {
        //
        $entrada =  Entradas::find($id);
        $entrada->titulo = $request->input('titulo');
        $entrada->contenido = $request->input('contenido');
        if ($request->input('estado') == 2)                             //si el estado es igual a unoo
        {
            $fecha_actual = Carbon::now()->format('y-m-d');
            $entrada->fecha_publicacion = $fecha_actual;
        }
        $entrada->estado = $request->input('estado');
        $entrada->categorias_id = $request->input('id_categoria');
        $entrada->save();

        return redirect('/entradas/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entradas  $entradas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $entrada = Entradas::find($id);
        $entrada->delete();

        return redirect('/entradas/');
    }
    /**
     * Metodo se encargar de devolver los ultimos registros de entradas agregados
     *
     */
    public function recientes()
    {
        /*----------------------------------------------------------------------------
      *                Ordenando los registros de la db
      * -------------------------------------------------------------------------------
      *- El "reverse" método invierte el orden de los elementos de la colección
      * traida de la base de datos
      *-luego de revertir el orden de los registros de la db slice, toma los primeros
      * 3 registros, comienza en la pocicion cero y apartir de ahi toma tres registros
      *------------------------------------------------------------------------------*/

        $entradas_orden_default = Entradas::where('estado','=','2')->get();
        $todas_entradas = $entradas_orden_default->reverse();

        $entradas_recientes = $todas_entradas->slice(0,3);
        $entradas_recientes->all();

        $categorias = Categorias::all();
        $comentarios = Comentarios::all();
        $users = User::all();

        return view('entradas.recientes')->with(compact('entradas_recientes','categorias',
            'comentarios','users'));
    }
    /**
     *  Metodo se encarga de guardar los comentarios de los usuarios
    */
    public function store_recientes(Request $request)
    {
        //
        $comentario = new Comentarios();
        $comentario->entrada_id = $request->input('entrada_id');
        $comentario->user_id = $request->input('user_id');
        $comentario->comentario = $request->input('comentario');
        $comentario->save();
        return redirect('/recientes/');

    }
}
