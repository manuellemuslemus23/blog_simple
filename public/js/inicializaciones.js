$(document).ready(function() {
    $(".dropdown-trigger").dropdown();
    $('select').formSelect();
    $('input#input_text, textarea#textarea2').characterCounter();

    /* inicializacion de boton flotante */
    $('.fixed-action-btn').floatingActionButton();

    $('.collapsible').collapsible();

    /* inicializacion para el datepicker y configuarion de lenguaje a espa;ol */
    $('.datepicker').datepicker({
        firstDay: true,
        format: 'yyyy-mm-dd',
        yearRange: 50,
        i18n: {
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
            weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
        }
    });

});
