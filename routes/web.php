<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 /*-------------------------------------------
 * rutas para los controladores de recursos  |
 * ------------------------------------------*/

Route::resources([
    'categorias' => 'CategoriasController',
    'entradas' => 'EntradasController',
    'users' => 'UsersController',
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*---------------------------------------------------
 * Rutas para entradas recientes y para comentarios
 --------------------------------------------------*/
Route::get('/recientes', 'EntradasController@recientes');
Route::post('/recientes', 'EntradasController@store_recientes');
