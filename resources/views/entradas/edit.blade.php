@extends('layouts.blog')
@section('title') Entradas | edit @endsection
@section('content')
    <br><br>
    <div class="card-panel z-depth-2">
        <h3 class="teal-text"><b>Editando Entrada : {{ $entrada->titulo }}</b></h3>
        <br>
        <form action="{{ url('/entradas/' . $entrada->id) }}" method="post">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">            <div class="row">
                <div class="row col s12 l6">
                    <h6 class="center-align">Estado de entrada</h6>
                    @if($entrada->estado == 1)
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="3"  class="filled-in"/>
                                <span>Despublicar</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="2" class="filled-in" />
                                <span>Publicacion</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="1" checked class="filled-in"/>
                                <span>Borrador</span>
                            </label>
                        </p>
                    @elseif($entrada->estado == 2)
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="3"  class="filled-in"/>
                                <span>Despublicar</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="2" checked class="filled-in" />
                                <span>Publicacion</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="1"  class="filled-in"/>
                                <span>Borrador</span>
                            </label>
                        </p>
                    @else
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="3" checked class="filled-in"/>
                                <span>Despublicar</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="2"  class="filled-in" />
                                <span>Publicacion</span>
                            </label>
                        </p>
                        <p class="col s4 center-align">
                            <label>
                                <input type="checkbox" name="estado" value="1"  class="filled-in"/>
                                <span>Borrador</span>
                            </label>
                        </p>
                    @endif
                </div>
                <div class="col s12 l6">
                    <br>
                    <div class="input-field col s12">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_categoria" >
                            <option value="{{ $categoriaActual->id }}">{{ $categoriaActual->nombre_categoria }}</option>
                            @foreach ($categorias as $categoria)
                                <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
                            @endforeach
                        </select>
                        <label>Categorias</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <label class=" ">Titulo</label>
                    <input id="icon_prefix" type="text" name="titulo" value="{{ $entrada->titulo }}" class="validate">
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <label class="black-text">Contenido</label>
                    <br>
                    <div class="input-field col s12">
                        <textarea id="textarea1" name="contenido" class="materialize-textarea">
                            {{ $entrada->contenido }}
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <bbr></bbr>
                <button class="btn col s12 l8 push-l2 btn" type="submit"><b>Guardar Nuevo</b></button>
                <br>
            </div>
        </form>
    </div>
    <!--boton flotante-->
    <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px; ">
        <a class="btn-floating btn-large waves-effect waves-light red" href="javascript:history.back()">
            <i class="large material-icons">keyboard_backspace</i>
        </a>
    </div>
    </div>
    <br> <br>
@stop
