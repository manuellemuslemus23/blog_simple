@extends('layouts.blog')
@section('title') Entradas | create @endsection
@section('content')
    <div class="card-panel z-depth-2">
        <h3 class="teal-text"><b>Crear Entrada</b></h3>
        <form action="{{ url('/entradas') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="row col s12 l6">
                    <h6 class="center-align">Estado de entrada</h6>
                    <p class="col s6 center-align">
                        <label>
                            <input type="checkbox" name="estado" value="2" class="filled-in" />
                            <span>Publicacion</span>
                        </label>
                    </p>
                    <p class="col s6 center-align">
                        <label>
                            <input type="checkbox" name="estado" value="1" class="filled-in"/>
                            <span>Borrador</span>
                        </label>
                    </p>
                </div>
                <div class="col s12 l6">
                    <br>
                    <div class="input-field col s12">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_categoria" >
                            @foreach ($categorias as $categoria)
                                <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
                            @endforeach
                        </select>
                        <label>Categorias</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <label class=" ">Titulo</label>
                    <input id="icon_prefix" type="text" name="titulo" class="validate">
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <label class="black-text">Contenido</label>
                    <br>
                    <div class="input-field col s12">
                        <textarea id="textarea1" name="contenido" class="materialize-textarea"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <button class="btn col s12 l8 push-l2 btn" type="submit"><b>Guardar Nuevo</b></button>
            </div>
        </form>
    </div>
    <!--boton flotante-->
    <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px; ">
        <a class="btn-floating btn-large waves-effect waves-light red" href="javascript:history.back()">
            <i class="large material-icons">keyboard_backspace</i>
        </a>
    </div>
    </div>
@stop
