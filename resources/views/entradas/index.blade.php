@extends('layouts.blog')
@section('title') Entradas @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <h2 class="teal-text"><b>Entradas</b></h2>
        <hr>
        <table class="highlight centered">
            <thead>
            <tr>
                <th>id</th>
                <th>Titulo</th>
                <th>Fecha publicacion</th>
                <th>Estado</th>
                <th>Categoria</th>
                <th colspan="2">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($entradas as $entrada)
                <tr>
                    <td>{{ $entrada->id }}</td>
                    <td>{{ $entrada->titulo }}</td>
                    <td>{{ $entrada->fecha_publicacion }}</td>
                    <td>
                        @if($entrada->estado == 1)
                            <label class="red-text">Borrador</label>
                        @elseif($entrada->estado == 2)
                            <label class="green-text">Publicada</label>
                        @else
                            <label class="blue-text">Despublicada</label>
                        @endif
                    </td>
                    <td>
                        @foreach($categorias as $categoria)
                            @if($categoria->id == $entrada->categorias_id)
                                {{ $categoria->nombre_categoria }}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        <form class="" action="{{ url('/entradas/'. $entrada->id) }}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar la entrada de : ' + '{{ $entrada->titulo }}' )" type="submit" name="action"
                                    data-position="top" data-tooltip="Eliminar">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <a href="{{ url('/entradas/'. $entrada->id .'/edit') }}" class="btn btn-success blue tooltipped center" data-position="top" data-tooltip="Editar">
                            <i class="material-icons">edit</i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!--boton flotante-->
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large waves-effect waves-light red" href="{{ url('/entradas/create') }}"><i class="material-icons">add</i></a>
        </div>
    </div>
@stop
