@extends('layouts.blog')
@section('title') Entradas @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2 grey lighten-2">
        <h2 class="teal-text "><b>Entradas recientes</b></h2>
            @foreach($entradas_recientes as $entrada)
            <div class="row">
                <div class="col s12 ">
                    <div class="card  darken-1 card-panel">
                        @foreach($categorias as $categoria)
                            @if($categoria->id == $entrada->categorias_id)
                                <label class="black-text"> {{ $categoria->nombre_categoria }} </label>
                            @endif
                        @endforeach
                        <label class="right black-text">Publicado <b>{{ $entrada->fecha_publicacion }}</b></label>
                        <div class="card-content black-text">
                            <h4 class=""><b>{{ $entrada->titulo }}</b></h4>
                            <p>{{ $entrada->contenido }}</p>
                        </div>
                            <div class="card-action grey lighten-2 row">
                                <form action="{{ url('/recientes') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    <input type="hidden" name="entrada_id" value="{{ $entrada->id }}">
                                    <div class="input-field col s10">
                                            <input  id="first_name2" type="text" name="comentario" class="validate">
                                            <label class="" for="first_name2">Dejar un comentario</label>
                                        </div>
                                    <div class="col s2">
                                        <br>
                                        <button class="btn waves-effect waves-light light-blue darken-3" type="submit">
                                            <b> Comentar<i class="large material-icons">navigate_next</i></b>
                                        </button>
                                    </div>
                                </form>

                                <ul class="collapsible col s12 white" data-collapsible="accordion">
                                    <li>
                                        <div class="collapsible-header"><i class="material-icons">chat</i>Comentarios</div>
                                        <div class="collapsible-body row">
                                            @foreach($comentarios as $comentario)
                                                @if($comentario->entrada_id == $entrada->id)
                                                    @foreach($users as $user)
                                                        @if($comentario->user_id == $user->id)
                                                           <b > {{$user->name }} :</b>
                                                        @endif
                                                    @endforeach
                                                    <p>{{ $comentario->comentario }}</p>
                                                        <hr>
                                                @endif
                                            @endforeach

                                        </div>
                                    </li>
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
            @endforeach

        <!--boton flotante-->
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large waves-effect waves-light red" href="{{ url('/entradas/create') }}"><i class="material-icons">add</i></a>
        </div>
    </div>
@stop
