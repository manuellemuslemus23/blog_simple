<!-- DropDown Sesion -->
<ul id="dropdown3" class="dropdown-content" style="padding: 5px; margin: 3px">
    <li>
        <a class="" href="{{ route('logout') }}"
           onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
<nav class="teal">
        <div class="nav-wrapper">
            <li class=" brand-logo  center"><b>BLOG SIMPLE</b></li>
            <ul class="right hide-on-med-and-down">
                @guest
                     <!--
                     <li class="">
                        <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    -->
                    @if (Route::has('register'))
                        <li>
                        <!--  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>-->
                        </li>
                    @endif
                @else
                    @if(Auth::user()->tipo == 1)
                    <li><a href="{{ url('/recientes') }}">Recientes</a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown3" value="{{ csrf_token() }}">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                    @elseif(Auth::user()->tipo == 2)
                        <li><a href="{{ url('/recientes') }}">Recientes</a></li>
                        <li><a href="{{ url('/entradas') }}">Entradas</a></li>
                        <li><a href="{{ url('/categorias') }}">Categorias</a></li>
                        <li><a class="dropdown-trigger" href="#!" data-target="dropdown3" value="{{ csrf_token() }}">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                    @endif
            </ul>
            @endguest

        </div>
</nav>
