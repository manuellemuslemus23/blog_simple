@extends('layouts.blog')
@section('title') Categorias @endsection
@section('content')
<br>
<div class="card-panel z-depth-2">
    <h2 class="teal-text"><b>Categorias</b></h2>
    <table class="highlight centered">
        <thead>
        <tr>
            <th>id</th>
            <th>Nombre Categoria</th>
            <th>Fecha Creacion</th>
            <th colspan="2">Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categorias as $categoria)
            <tr>
                <td>{{ $categoria->id }}</td>
                <td>{{ $categoria->nombre_categoria }}</td>
                <td>{{ $categoria->fecha_cracion }}</td>
                <td>
                    <form class="" action="{{ url('/categorias/'. $categoria->id) }}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar la categoria de : ' + '{{ $categoria->nombre_categoria }}' )" type="submit" name="action"
                                data-position="top" data-tooltip="Eliminar">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </td>
                <td>
                    <a href="{{ url('/categorias/'. $categoria->id .'/edit') }}" class="btn btn-success blue tooltipped center" data-position="top" data-tooltip="Editar">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!--boton flotante-->
    <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large waves-effect waves-light red" href="{{ url('/categorias/create') }}"><i class="material-icons">add</i></a>
    </div>
</div>


@stop
