@extends('layouts.blog')
@section('title') Categorias | create @endsection
@section('content')
<br>
<div class="card-panel z-depth-2">
    <h3 class="teal-text"><b>Editando Categoria : {{ $categoria->nombre_categoria }}</b></h3>
    <form action="{{ url('/categorias/' . $categoria->id ) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="input-field col s12 l6">
                <i class="material-icons prefix">account_circle</i>
                <input id="icon_prefix" type="text" name="nombre_categoria" value="{{ $categoria->nombre_categoria }}" class="validate">
                <label for="icon_prefix">Nombre Categoria</label>
            </div>
            <div class="input-field col s12 l6">
                <i class="material-icons prefix">cake</i>
                <input  type="text" name="fecha_creacion" value="{{ $categoria->fecha_cracion }}" class="datepicker" required>
                <label>Fecha de Creacion</label>
            </div>
        </div>
        <div class="row">
            <button class="btn col s12 l8 push-l2 btn" type="submit"><b>Guardar Nuevo</b></button>
        </div>
    </form>
</div>
<!--boton flotante-->
<div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px; ">
    <a class="btn-floating btn-large waves-effect waves-light red" href="javascript:history.back()">
        <i class="large material-icons">keyboard_backspace</i>
    </a>
</div>
</div>
@stop
