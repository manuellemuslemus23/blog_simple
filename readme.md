<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Blog Simple

* Página de inicio, con las 3 entradas (posts )más recientes y el listado de categorías para poder navegar en ellas.
* Página de categoría; es decir, listado de las entradas correspondientes a cada categoría.
* Página de detalle de la entrada.
* Los usuarios podrán dejar comentarios en las distintas entradas del blog

## Requerimientos
* php 7.1
* MySql 5.7
* Apache 2
* Composer
* Phpunit

### configuraciones:
luego de clonar el proyecto, lo primero que debemos hacer es entrar a la carpeta que clonamos y abrir una terminal dentro
de ella, luego escribir
 
	$ composer update

	
Posteriormente debemos abrir el archivo .env y editar nuestra conexion a la base de datos, tanto el usuario, la contraseña
como tambien el nombre de la base de datos que utilizaremos (para este paso ya debe haber creado la base de datos en mysql)

busque en el archivo env el siguiente codigo: 
Liego cambiar los valores por los correspondientes
	
	DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=nombre_de_base_de_datos
    DB_USERNAME=usuario
    DB_PASSWORD=contrasena

Por ultimo solo debemos ejecutar las migraciones que ya estan echas, estas crearan las tablaas en la base de datos que 
indicamos hace un momento en el archivo .env, para ello escribiremos en la terminal lo siguiente:

    $ php artisan migrate
      
Luego incertaremos datos de prueba, tanto un usuario como, categorias y entredas
ejecutamos en la terminal:

    $ php artisan db:seed

si todo salio bien, ahora puede ejecutar las pruebas, solo debe escribir en la terminal lo siguiente :

    $ vendor/bin/phpunit
    
Ahora solo levantamos el servidor:

     $ php artisan serve

Cuando creamos los datos de prueba, automaticamente creamos un suario para iniciar sesion
con estos datos:

 email : admin@gmail.com'
 
 password : laravel123


Detalle: en la carpeta public/img/ podremos encontrar una imagen con el diagrama de la base de datos
con el nombre de diagrama.png
## Desarrollador

 Manuel Angel Lemus
