<?php

use Illuminate\Database\Seeder;
use App\Entradas;
use Illuminate\Support\Facades\DB;

class EntradasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     */
    public function run()
    {
        //
        factory(Entradas::class, 15)->create();

    }
}
