<?php
use App\Categorias;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Categorias::class, 10)->create();
    }
}
