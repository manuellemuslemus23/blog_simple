<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Categorias;
use Faker\Generator as Faker;

$factory->define(Categorias::class, function (Faker $faker) {
    return [
        //
        'nombre_categoria' => $faker->name,
        'fecha_cracion' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});
