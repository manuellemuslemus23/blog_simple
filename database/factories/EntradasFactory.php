<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entradas;
use Faker\Generator as Faker;

$factory->define(Entradas::class, function (Faker $faker) {
    return [
        //
        'titulo' => $faker->name,
        'contenido' => $faker->text($maxNbChars = 200),
        'fecha_publicacion' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'estado' => $faker->numberBetween($min = 1, $max = 3),
        'categorias_id' =>  $faker->numberBetween($min = 1, $max = 10),
    ];
});
