<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class CategoriasTest extends TestCase
{
    /**
     * test de view index create
     */
    public function testIndex()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }
    /**
     * test de view formulario create
     */
    public function testUser()
    {
        $user = factory(User::class)->create();

        //autenticando ususario y manteniendo la sesion
        $response = $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get('/');
        //esta es la assercion que se repite, esta asi para que no salte una advertencia al ejecutar las pruebas
        $response->assertStatus(200);
    }
    public function testFormCreate()
    {
        $this->testUser();
        $response = $this->get('/categorias/create');

        $response->assertStatus(200);
    }
    public function testFormEdit()
    {
        $this->testUser();
        $response = $this->get('/categorias/1/edit');
        $response->assertStatus(200);
    }
 }
